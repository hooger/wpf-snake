﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SnakeLibrary
{
    public class Food
    {
        public int ID { get; set; }
        public Rect Location { get; set; }
        public int UpperBounds { get; set; }
        public int LowerBounds { get; set; }
        public int Size { get; set; }

        Random rnd = new Random(); 

        public Food(int upper, int lower, int size)
        {
            UpperBounds = upper;
            LowerBounds = lower;
            Size = size;

            newLocation();
        }

        public void newLocation()
        {
            int x = rnd.Next(LowerBounds, UpperBounds);
            int y = rnd.Next(LowerBounds, UpperBounds);
            Location = new Rect(x, y, Size, Size);
        }

        public bool checkPlayerCollision(Dictionary<int, Snake> players)
        {
            List<Rect> playerLocations = new List<Rect>();
            foreach (var player in players.Values)
            {
                for (int i = 0; i < player.Location.Count; ++i)
                    playerLocations.Add(player.Location[i]);
            }

            for (int i = 0; i < playerLocations.Count; ++i )
            {
                if (Location.IntersectsWith(playerLocations[i]))
                    return true;
            }
            return false;
        }
    }
}
