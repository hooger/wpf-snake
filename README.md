# README #

Fanashawe College
Semester 5
Component Programming .NET

**Project 2 - Multiplayer Snake**

### What is this repository for? ###

* A multiplayer 'Snake' game
* Using .NET's WCF to communicate over the local network, you can 
currently connect up to 8 players per game.  That number is scalable and 
setable in 'Game.cs'
* Objective of the game is to capture the food 30 times without hitting 
yourself, a wall, or another player.
* 1.0.5

### How do I get set up? ###

* git Clone
* Build using Visual Studio 2012
* Needed files are the server.exe, and the client.exe, and the 
config.exe for both of them.
* Run the server, then the client and then connect.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
